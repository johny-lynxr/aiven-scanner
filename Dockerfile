FROM python:3.8

COPY . /aiven-scanner
EXPOSE 9999

WORKDIR /aiven-scanner
RUN pip install -r requirements.txt
CMD python main.py