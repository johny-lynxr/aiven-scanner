from unittest.mock import AsyncMock


class AsyncContenxtManagerMock(AsyncMock):

    async def __aenter__(self, *args, **kwargs):
        return AsyncContenxtManagerMock()

    async def __aexit__(self, *args, **kwargs):
        return True

    def __await__(self):
        async def wrapper(*args, **kwargs):
            return self

        return wrapper().__await__()


class FakeDbPool:

    def __init__(self, *args, **kwargs) -> None:
        self.args = args
        self.kwargs = kwargs

    def __await__(self):
        async def wrapper(*args, **kwargs):
            return self

        return wrapper().__await__()

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        return True

    def acquire(self, *args, **kwargs):
        return FakeDbPool(*args, **kwargs)

    def cursor(self, *args, **kwargs):
        return self

    async def execute(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

    async def fetchone(self, *args, **kwargs):
        return (args, kwargs)
