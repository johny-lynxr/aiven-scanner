import asyncio
from unittest import mock

import aiounittest

from app.fetcher.base import HttpFetcher, HttpFetcherResponse


class TestFetcher(aiounittest.AsyncTestCase):

    def setUp(self) -> None:
        self.fetcher = HttpFetcher()

    def tearDown(self) -> None:
        del HttpFetcher._instance

    def get_event_loop(self):
        return asyncio.get_event_loop()

    async def test_fetch_success(self):
        resp = await self.fetcher.fetch({'url': 'http://google.com'})
        self.assertIsInstance(resp, HttpFetcherResponse)
        self.assertNotEqual(resp.status_code, -1)

    async def test_fetch_exception(self):
        with mock.patch('aiohttp.ClientSession.get') as get:
            get.side_effect = Exception()
            resp = await self.fetcher.fetch({'url': 'http://google.com'})
            self.assertIsInstance(resp, HttpFetcherResponse)
            self.assertEqual(resp.status_code, -1)
