import asyncio
from unittest import mock
from unittest.mock import AsyncMock, MagicMock

import aiounittest

from app.kafka_connector import KafkaConnector


class TestKafkaConnector(aiounittest.AsyncTestCase):

    def setUp(self) -> None:
        super().setUp()
        self.consumer_mock = AsyncMock()
        self.patchers = [mock.patch('app.kafka_connector.base.AIOKafkaProducer', return_value=AsyncMock()),
                         mock.patch('app.kafka_connector.base.AIOKafkaConsumer', return_value=self.consumer_mock)]
        list(map(lambda x: x.start(), self.patchers))

    def get_event_loop(self):
        return asyncio.get_event_loop()

    @staticmethod
    async def ex(*args, **kwargs):
        raise Exception()

    async def test_producer(self):
        config_mock = MagicMock()
        config_mock.type = 'producer'
        kafka_connector = KafkaConnector(config_mock)
        await kafka_connector.start(sites=[dict(url='http://fake.fake', interval=5)], db=AsyncMock())
        await asyncio.wait([kafka_connector.join()], timeout=6)
        kafka_connector.producer.send_and_wait.assert_called()
        kafka_connector.stop(asyncio.get_event_loop())
        self.assertTrue(kafka_connector.stop_event.is_set())

    async def test_producer_exception(self):
        config_mock = MagicMock()
        config_mock.type = 'producer'
        kafka_connector = KafkaConnector(config_mock)

        kafka_connector.producer.start = self.ex
        tasks = await kafka_connector.start(sites=[dict(url='http://fake.fake', interval=5)], db=AsyncMock())
        with self.assertRaises(Exception):
            await tasks[0]
        kafka_connector.producer.send_and_wait.assert_not_called()
        self.assertFalse(kafka_connector.stop_event.is_set())

    async def test_consumer(self):
        self.consumer_mock.__aiter__.return_value = [MagicMock()]
        config_mock = MagicMock()
        config_mock.type = 'consumer'
        kafka_connector = KafkaConnector(config_mock)
        db_mock = AsyncMock()
        await kafka_connector.start(sites=[dict(url='http://fake.fake', interval=5)], db=db_mock)
        await asyncio.wait([kafka_connector.join()], timeout=6)
        db_mock.insert_check_results.assert_called()

    async def test_consumer_exception(self):
        site = dict(url='http://fake.fake', interval=5)
        # message_mock = MagicMock()
        # message_mock.__anext__.return_value = MagicMock()
        # self.consumer_mock.__aiter__.side_effect = [message_mock]
        config_mock = MagicMock()
        config_mock.type = 'consumer'
        kafka_connector = KafkaConnector(config_mock)
        kafka_connector.consumer.start = self.ex
        # db_mock = AsyncMock()
        # db_mock.insert_check_results.side_effect = self.ex
        tasks = await kafka_connector.start(sites=[site], db=AsyncMock())
        with self.assertRaises(Exception):
            await tasks[0]
        a = 1