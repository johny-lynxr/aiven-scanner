from unittest import mock
from unittest.mock import AsyncMock, MagicMock

import aiounittest

from app.db_connector import DbConnector
from tests.base import AsyncContenxtManagerMock, FakeDbPool


class TestDbConnector(aiounittest.AsyncTestCase):
    host = 'fake_host'
    port = 123
    user = 'fake_user'
    password = 'fake_pass'
    db = 'fake_db'

    def setUp(self) -> None:
        self.db_mock = AsyncContenxtManagerMock()

        self.db_mock.acquire = AsyncMock()
        self.db_mock.acquire.side_effect = AsyncContenxtManagerMock()
        self.aiopg_patch = mock.patch("aiopg.create_pool", side_effect=FakeDbPool)
        self.aoipg_mock = self.aiopg_patch.start()

    def tearDown(self) -> None:
        self.aiopg_patch.stop()

    async def __create_db_connector(self):
        return await DbConnector.create(self.host, self.port, self.user, self.password, self.db)

    def test_make_dsn(self):
        dsn = DbConnector.make_dsn(self.host, self.port, self.user, self.password, self.db)
        self.assertEqual(dsn, 'dbname=fake_db user=fake_user password=fake_pass host=fake_host port=123')

    async def test_create(self):
        await self.__create_db_connector()
        self.aoipg_mock.assert_called_once()
        self.aoipg_mock.assert_called_with(
            f'dbname={self.db} user={self.user} password={self.password} host={self.host} port={self.port}')

    async def test_create_cursor(self):
        db_connector = await self.__create_db_connector()
        async with db_connector.get_cursor() as cur:
            self.assertIsInstance(cur, FakeDbPool)

    async def test_execute(self):
        db_connector = await self.__create_db_connector()
        args = ("SELECT * FROM FAKE WHERE id=%(id)s", {'id': 1})
        async with db_connector.execute(*args) as resp:
            self.assertEqual(resp, args)
        resp = await db_connector.get_sites_need_check()
        self.assertEqual(resp, [])
        resp = await db_connector.insert_check_results(MagicMock())
        self.assertEqual(resp, None)
        resp = await db_connector.get_now()
        self.assertEqual(resp, None)
