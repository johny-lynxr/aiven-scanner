# Aiven scanner

## Installation 
```shell script
docker run -ti -d -v <PATH_TO_CONFIG_JSON_DIR>:/etc/aiven-scanner lynxr/aiven-scanner
```

## Example config file

```json
{
  "server": {
    "reload_port": 9999
  },
  "database": {
    "host": "localhost",
    "port": 5432,
    "user": "postgres",
    "password": "password",
    "db": "aiven"
  },
  "kafka": {
    "type": "both",
    "bootstrap_servers": "localhost:9092",
    "group_id": "test1",
    "topic": "aiven2"
  },
  "logging": {
    "format": "%(asctime)-15s [%(name)s %(levelname)s]: %(message)s",
    "level": "DEBUG"
  }
}
```