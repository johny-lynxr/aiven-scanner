import asyncio
import logging
import signal
import sys
from argparse import ArgumentParser
from functools import partial

from python_json_config import ConfigBuilder

from app.db_connector import DbConnector
from app.kafka_connector import KafkaConnector


async def main():
    """
    Entrypoint
    Parsing arguments, config
    Initializing database pool
    Starting kafka Producer/consumer
    :return:
    """
    args_parser = ArgumentParser()
    args_parser.add_argument('--config', default='/etc/aiven-scanner/config.json', type=str)
    args = args_parser.parse_args()

    parser = ConfigBuilder()
    config = parser.parse_config(args.config)

    logging.basicConfig(format=config.logging.format, level=logging.getLevelName(config.logging.level))

    database_settings = config.database
    db_connector = await DbConnector.create(database_settings.host, database_settings.port,
                                            database_settings.user, database_settings.password, database_settings.db)
    kafka = KafkaConnector(config.kafka)
    sites = await db_connector.get_sites_need_check()
    await kafka.start(sites=sites, db=db_connector)
    kafka_stop = partial(kafka.stop, loop=asyncio.get_event_loop())

    async def reload_sites_callback(reader, writer):
        try:
            kafka_stop()
            sites = await db_connector.get_sites_need_check()
            await kafka.start(sites=sites, db=db_connector)
            writer.write(b'OK')
        except Exception as e:
            writer.write(b'FAIL')
        finally:
            await writer.drain()
            writer.close()

    await asyncio.start_server(reload_sites_callback, '0.0.0.0', config.server.reload_port)

    def shutdown(*args, **kwargs):
        code = kafka_stop()
        for task in asyncio.all_tasks():
            task.cancel()
        sys.exit(code)

    asyncio.get_event_loop().add_signal_handler(signal.SIGTERM, shutdown)
    asyncio.get_event_loop().add_signal_handler(signal.SIGINT, shutdown)


if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())
    asyncio.get_event_loop().run_forever()
