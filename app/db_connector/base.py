import os
from contextlib import asynccontextmanager

import aiopg
import psycopg2.extras


class DbConnector:
    """
    Database connector.
    Contains all methods to select/insert data
    """

    @staticmethod
    def make_dsn(host, port, user, password, db):
        """

        :param host: str database host
        :param port: int database port
        :param user: str database user
        :param password: str database password
        :param db: str database name
        :return: str - dsn to connect to database
        """
        return f'dbname={db} user={user} ' \
               f'password={password} host={host} port={port}'

    @classmethod
    async def create(cls, host, port, user, password, db, **kwargs):
        """
        Factory Method to create DbConnector instance

        usage:
            db_connector = await DbConnector.create(...)

        :param host: str database host
        :param port: int database port
        :param user: str database user
        :param password: str database password
        :param db: str database name
        :param kwargs: additional arguments when create database connection
        :return: DbConnector class instance
        """
        dsn = cls.make_dsn(host, port, user, password, db)
        connector = cls(dsn)
        await connector.__initialize(**kwargs)
        return connector

    def __init__(self, dsn) -> None:
        self.dsn = dsn
        self.pool = None

    async def __initialize(self, **kwargs):
        self.pool = await aiopg.create_pool(self.dsn, **kwargs)
        with open(os.path.join(os.path.abspath('db.sql')), 'r') as file:
            sql = file.read()
        async with self.execute(sql) as resp:
            pass

    @asynccontextmanager
    async def get_cursor(self):
        """
        Wrapper for creating database cursor
        :return:
        """
        async with self.pool.acquire() as conn:
            async with conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cur:
                yield cur

    @asynccontextmanager
    async def execute(self, sql, *args):
        """
        Provides interface to execute any sql without creating connections/cursors manually
        :param sql: str SQL query
        :param args: tuple query arguments
        :return:
        """
        async with self.get_cursor() as cur:
            await cur.execute(sql, *args)
            yield cur

    async def get_sites_need_check(self):
        """
        Returning list of sites which are enabled
        :return: list[dict] sites to parse
        """
        sites = []
        async with self.execute(
                """SELECT * FROM sites WHERE enabled is TRUE""") as resp:
            async for row in resp:
                sites.append(row)
        return sites

    async def insert_check_results(self, data):
        """
        Inserting results of checking site
        :param data: dict data to insert
        :return:
        """
        async with self.execute(
                """INSERT INTO check_logs (site_id, status_code, parsed_body, response_time, parsed_at) VALUES (%(site_id)s, %(status_code)s, %(parsed_body)s, %(response_time)s, %(parsed_at)s)""",
                data) as resp:
            if resp.rowcount != 1:
                raise Exception("error insert data to database")

    async def get_now(self):
        """
        Getting CURRENT_TIMESTAMP from database
        :return: datetime timestamp
        """
        async with self.execute("SELECT CURRENT_TIMESTAMP as t") as resp:
            data = await resp.fetchone()
            return data.get('t')
