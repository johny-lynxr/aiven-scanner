import asyncio
import logging
import re
from dataclasses import dataclass
from typing import Optional

import aiohttp


class Singleton(type):
    """
    Stolen from here
    https://stackoverflow.com/questions/31269974/why-singleton-in-python-calls-init-multiple-times-and-how-to-avoid-it
    """
    _instance = None

    def __call__(self, *args, **kwargs):
        if not self._instance:
            self._instance = super(Singleton, self).__call__()
        return self._instance


class HttpFetcher(metaclass=Singleton):
    """
    Getting required data from site
    """

    logger = logging.getLogger('HttpFetcher')

    def __init__(self) -> None:
        self.trace_config = aiohttp.TraceConfig()
        self.trace_config.on_request_start.append(self.__on_request_start)
        self.trace_config.on_request_end.append(self.__on_request_end)
        self.connector = aiohttp.TCPConnector(limit=0)
        self.session = aiohttp.ClientSession(loop=asyncio.get_event_loop(),
                                             trace_configs=[self.trace_config], connector=self.connector)

    @staticmethod
    async def __on_request_start(session, trace_config_ctx, params):
        """
        Save current loop time to request context when request start
        :param session: aiohttp.ClientSession instance
        :param trace_config_ctx: aiohttp.TraceConfig instance
        :param params: tuple additional params
        :return:
        """
        trace_config_ctx.start = asyncio.get_event_loop().time()

    @staticmethod
    async def __on_request_end(session, trace_config_ctx, params):
        """
        Save to current request context request time
        :param session: aiohttp.ClientSession instance
        :param trace_config_ctx: aiohttp.TraceConfig instance
        :param params: tuple additional params
        :return:
        """
        trace_config_ctx.trace_request_ctx['time'] = asyncio.get_event_loop().time() - trace_config_ctx.start

    async def fetch(self, site):
        """
        Fetching given site and wrapping results to object
        :param site: dict site object from database
        :return: HttpFetcherResponse object
        """
        self.logger.debug(f'start fetching site {site.get("url")}')
        ctx = dict()
        try:
            async with self.session.get(site.get('url'), trace_request_ctx=ctx) as resp:
                found = None
                if site.get('regex'):
                    body = await resp.text()
                    pattern = re.compile(site.get('regex'))
                    found = '\n'.join(pattern.findall(body))
                    found = found if found.strip() else None
                self.logger.debug(f"{site.get('url')} fetched, response time == {ctx.get('time')}")
                return HttpFetcherResponse(dict(site), ctx.get('time'), found, resp.status)
        except Exception as e:
            self.logger.error(str(e))
            return HttpFetcherResponse(dict(site), 0, None, -1)


@dataclass
class HttpFetcherResponse:
    """
    Response data class
    """
    site: dict
    response_time: float
    found: Optional[str]
    status_code: int
