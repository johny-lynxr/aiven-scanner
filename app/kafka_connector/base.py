import asyncio
import logging

from aiokafka import AIOKafkaProducer, AIOKafkaConsumer

from app.kafka_connector.serializers import serializer, deserializer
from app.kafka_connector.workers import KafkaProducerWorker, KafkaConsumerWorker


class KafkaConnector:
    """
    Kafka wrapper for producing/consuming data via workers
    """

    logger = logging.getLogger("KafkaConnector")

    default_consumer_worker_cls = KafkaConsumerWorker
    default_producer_worker_cls = KafkaProducerWorker

    def __init__(self, config, **kwargs) -> None:
        """
        :param config: kafka config
        :param kwargs:
            - serializer: serialize function
            - deserializer: deserialize function
            - producer_worker_cls: subclass of :class: KafkaProducerWorker to process data
            - consumer_worker_cls: subclass of :class: KafkaConsumerWorker to process data
        """
        self.config = config
        self.tasks = []
        self.producer = AIOKafkaProducer(loop=asyncio.get_event_loop(), bootstrap_servers=self.config.bootstrap_servers,
                                         value_serializer=kwargs.get('serializer', serializer))
        self.consumer = AIOKafkaConsumer(self.config.topic, loop=asyncio.get_event_loop(),
                                         bootstrap_servers=self.config.bootstrap_servers,
                                         value_deserializer=kwargs.get('deserializer', deserializer),
                                         group_id=self.config.group_id,
                                         auto_offset_reset='earliest', enable_auto_commit=False)
        self.stop_event = asyncio.Event()
        self.producer_worker_cls = kwargs.get('producer_worker_cls', self.default_producer_worker_cls)
        self.consumer_worker_cls = kwargs.get('consumer_worker_cls', self.default_consumer_worker_cls)

    async def start_produce(self, data, **worker_kwargs):
        """
        Start producing data using producer_worker_cls
        :param data: iterable data to process
        :param worker_kwargs: additional arguments to put to worker
        :return:
        """
        self.logger.info("Start kafka producer")
        await self.producer.start()
        self.tasks = [asyncio.get_event_loop().create_task(
            self.producer_worker_cls(self, x, **worker_kwargs).process()) for x in data]

    async def start_consume(self, **worker_kwargs):
        """
        Start consuming data using consumer_worker_cls
        :param worker_kwargs: additional arguments to put to worker
        :return:
        """
        self.logger.info("Start kafka consumer")
        await self.consumer.start()
        async for msg in self.consumer:
            try:
                if self.stop_event.is_set():
                    break
                await self.consumer_worker_cls(self, msg, **worker_kwargs).process()
                await self.consumer.commit()
            except Exception as e:
                self.logger.exception(e)

    async def start(self, *args, **kwargs):
        """
        Starting kafka workers
        :param args:
        :param kwargs: data for workers
        :return: list asyncio.Task objects
        """
        tasks = []
        self.stop_event.clear()
        if self.config.type == 'producer' or self.config.type == 'both':
            sites = kwargs.pop('sites', None)
            assert sites, 'sites keyword argument required when config type in (producer, both)'
            tasks.append(self.start_produce(sites, **kwargs))
        if self.config.type == 'consumer' or self.config.type == 'both':
            db = kwargs.pop('db', None)
            assert db, 'db keyword argument required for config type in (consumer, both)'
            tasks.append(self.start_consume(db=db, **kwargs))
        return [asyncio.get_event_loop().create_task(x) for x in tasks]

    async def join(self):
        await self.stop_event.wait()

    def stop(self, loop):
        """
        stopping kafka workers
        :param loop: asyncio loop
        :return: status code
        """
        try:
            self.stop_event.set()
            for task in self.tasks:
                task.cancel()
            self.logger.info("Stopping kafka connector")
            return 0
        except:
            return -1
