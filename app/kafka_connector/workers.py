import asyncio
import datetime
import logging

from app.fetcher.base import HttpFetcher


class KafkaProducerWorker:
    """
    Worker class
    To implement your own worker extend this class and override method "process"
    """

    def __init__(self, connector, site, **kwargs) -> None:
        self.connector = connector
        self.topic = connector.config.topic
        self.producer = connector.producer
        self.site = site
        self.kwargs = kwargs
        self.logger = logging.getLogger(f"KafkaProducerWorker[{site.get('url')}]")

    async def process(self):
        self.logger.debug("Starting worker")
        fetcher = HttpFetcher()
        while not self.connector.stop_event.is_set():
            # to exclude fetching time create sleep here and await later
            sleep = asyncio.sleep(self.site.get('interval'))
            try:
                resp = await fetcher.fetch(self.site)
                self.logger.debug(f"{self.site.get('url')} fetch completed")
                await self.producer.send_and_wait(self.topic, resp)
            except Exception as e:
                self.logger.exception(e)
            finally:
                await sleep

    def __del__(self):
        self.logger.debug(f"KafkaProducerWorker[{self.site.get('url')}] stopped")


class KafkaConsumerWorker:
    """
    Worker class
    To implement your own worker extend this class and override method "process"
    """

    def __init__(self, connector, msg, **kwargs) -> None:
        self.connector = connector
        self.msg = msg
        self.kwargs = kwargs
        self.logger = logging.getLogger(f"KafkaConsumerWorker[{msg.value.site.get('url')}]")

    async def process(self):
        self.logger.debug("Starting worker")
        data = {
            'site_id': self.msg.value.site.get('id'),
            'status_code': self.msg.value.status_code,
            'parsed_body': self.msg.value.found,
            'response_time': self.msg.value.response_time,
            'parsed_at': datetime.datetime.utcfromtimestamp(self.msg.timestamp / 1000)
        }
        try:
            await self.kwargs.get('db').insert_check_results(data)
        except Exception as e:
            self.logger.error("Cant insert database results")
        finally:
            await asyncio.sleep(0)

    def __del__(self):
        self.logger.debug(f"KafkaConsumerWorker[{self.msg.value.site.get('url')}] stopped")
