import pickle


def serializer(value):  # pragma: no cover
    return pickle.dumps(value)


def deserializer(serialized):  # pragma: no cover
    return pickle.loads(serialized)
