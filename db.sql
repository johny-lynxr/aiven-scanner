create table if not exists public.sites
(
	id serial not null
		constraint sites_pk
			primary key,
	url varchar(1024) not null,
	interval integer default 60 not null,
	enabled boolean default true not null,
	regex text
);

create unique index if not exists sites_url_uindex
	on public.sites (url);

create table if not exists public.check_logs
(
	id bigserial not null
		constraint check_logs_pk
			primary key,
	site_id integer not null
		constraint check_logs_sites_id_fk
			references public.sites,
	status_code integer not null,
	parsed_body text,
	response_time double precision not null,
	created_at timestamp with time zone default CURRENT_TIMESTAMP not null,
	parsed_at timestamp with time zone not null
);

create index if not exists check_logs_status_code_index
	on public.check_logs (status_code);

